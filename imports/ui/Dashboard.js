import React, { Component } from 'react';
import PrivateHeader from './PrivateHeader';


export default () => {
    return (
        <div>
            <PrivateHeader title="Dashboard" />
            <div className="page-content">
                Dashboard Content here
            </div>
        </div>
    );
};